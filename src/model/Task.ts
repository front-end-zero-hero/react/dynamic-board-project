import React from "react";

export interface ITask {
    id: number;
    title: string;
    body: string;
    lane: number;
}
