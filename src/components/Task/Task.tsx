import styled from "styled-components";
import React, {ReactElement} from "react";

const TaskWrapper = styled.div`
    background: darkgray;
    padding: 20px;
    border-radius: 20px;
    margin: 0% 5% 5% 5%;
`
const Title = styled.h3 `
    width: 100%;
    margin: 0;
`
interface IProps {
    id: number;
    title: string;
    body: string;
    onDragStart: (e: React.DragEvent<HTMLDivElement>, id: number) => void;
}

const Task: React.FC<IProps> = ({id, title, body, onDragStart}): ReactElement  => {
    return(
        <TaskWrapper draggable onDragStart={(e) => onDragStart(e, id)}>
            <Title>{title}</Title>
            <p>{body}</p>
        </TaskWrapper>
    )
}

export default Task;
