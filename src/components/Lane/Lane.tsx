import Task from "../Task/Task";
import styled from "styled-components";
import React, {ReactElement} from "react";
import './Lane.css';
import {ITask} from "../../model/Task";

const LaneWrapper = styled.div`
  text-align: left;
  padding: 0;
  background: lightGray;
  border-radius: 20px;
  min-height: 50vh;
  width: 20vw;

  @media (max-width: 768px) {
    margin-bottom: 5%;
  }
`;

const Title = styled.h2`
  width: 100%;
  padding-bottom: 10px;
  text-align: center;
  border-bottom: 1px solid darkGray;
`;

interface IProps {
    laneId: number,
    title: string,
    loading: boolean,
    error: string,
    tasks: ITask[],
    onDragStart: (event: React.DragEvent<HTMLDivElement>, id: number) => void,
    onDragOver: (event: React.SyntheticEvent) => void,
    onDrop: (event: React.DragEvent<HTMLDivElement>, id: number) => void,
}

const Lane: React.FC<IProps> = ({laneId, title, loading, error, tasks, onDragStart, onDragOver, onDrop}): ReactElement => {
    return (
        <LaneWrapper onDragOver={onDragOver} onDrop={(e) => onDrop(e, laneId)}>
            <Title>{title}</Title>
            {loading || error ? (
                <span>{error || 'Loading...'}</span>
            ) : (
                tasks.map((task: ITask) => (
                    <Task
                        key={task.id}
                        id={task.id}
                        title={task.title}
                        body={task.body}
                        onDragStart={onDragStart}
                    />
                ))
            )}
        </LaneWrapper>
    )
}

export default Lane;

