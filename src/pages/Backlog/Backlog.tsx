import useDataFetching from "../../hooks/useDataFetching";
import Task from "../../components/Task/Task";
import React, {ReactElement} from "react";
import './Backlog.css';

const onDragStart = (e: React.DragEvent<HTMLDivElement>, id: number): void => {
    e.dataTransfer.setData('id', id.toString());
}

const Backlog: React.FC = (): ReactElement => {
    const [loading, tasks, error] = useDataFetching('https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/tasks');

    return (
        <div className={"Backlog-wrapper"}>
            <h2>Backlog</h2>
            <div className={"Tasks-wrapper"}>
                {loading || error ? (
                    <span>{error || 'Loading...'}</span>
                ) : (tasks.map((task) => (<Task key={task.id} id={task.id} title={task.title} body={task.body} onDragStart={onDragStart}/>)))}
            </div>
        </div>
    )
}

export default Backlog;
