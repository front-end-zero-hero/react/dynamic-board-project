import React, {useEffect, useState} from "react";
import useDataFetching from "../../hooks/useDataFetching";
import Lane from "../../components/Lane/Lane";
import {ITask} from "../../model/Task";
import './Board.css';

interface ILanes {
    id: number,
    title: string
}

const lanes: ILanes[] = [
    { id: 1, title: 'To Do' },
    { id: 2, title: 'In Progress' },
    { id: 3, title: 'Review' },
    { id: 4, title: 'Done' },
]

const onDragStart = (e: React.DragEvent<HTMLDivElement>, id: number): void => {
    e.dataTransfer.setData('id', id.toString());
}

const onDragOver = (events: React.SyntheticEvent) => {
    events.preventDefault();
}

const Board: React.FC = () => {
    const [loading, data, error] = useDataFetching(
        'https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/tasks'
    );

    const [tasks, setTasks] = useState<ITask[]>([]);

    useEffect(() =>{
        setTasks(data);
    }, [data]);

    const onDrop = (e: React.DragEvent<HTMLDivElement>, laneId: number) => {
        const id = e.dataTransfer.getData('id');

        const updatedTasks = tasks.filter((task) => {
            if (task.id.toString() === id) {
                task.lane = laneId;
            }
            return task;
        })

        setTasks(updatedTasks);
    }

    return (
        <div className={"Board-wrapper"}>
            {
                lanes.map((lane) => (
                    <Lane
                        key={lane.id}
                        laneId={lane.id}
                        title={lane.title}
                        loading={loading}
                        error={error}
                        tasks={tasks.filter((task) => task.lane === lane.id)}
                        onDragStart={onDragStart}
                        onDragOver={onDragOver}
                        onDrop={onDrop}
                    />
                ))
            }
        </div>
    );
}

export default Board;
