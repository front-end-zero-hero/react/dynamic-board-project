import {useEffect, useState} from "react";
import {ITask} from "../model/Task";

const useDataFetching = (dataSource: string):[boolean, ITask[], string] => {
    const [loading, setLoading] = useState<boolean>(false);
    const [data, setData] = useState<ITask[]>([]);
    const [error, setError] = useState<string>('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await fetch(dataSource);
                const result = await data.json();

                if(result) {
                    setData(result);
                    setLoading(false);
                }
            }
            catch (e: any) {
                setLoading(false);
                setError(e.message);
            }
        }

        fetchData().then();
    }, [dataSource]);

    return [loading, data, error];
}

export default useDataFetching;
